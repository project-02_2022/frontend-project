import React from "react";
// import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Dashboard from "./User/Dashboard/Dashboard";
import Newconference from "./User/NewConference/Newconference";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Signin from "./Login/Signin";
import Auth from "./Author/auth";
import Paper from "./Author/Paper/Paper";
import Regirationfee from "./RegirationFee/Regirationfee";
import ConferenceDetail from "./User/ConferenceDetail/ConferenceDetail";
import Test from "./RegirationFee/Test";
import FDashboard from "./User/Finance/FDashboard";
import PaperDetail from "./User/ConferenceDetail/PaperDetail";
import UPaper from "./User/UserPaper/UPaper";
import PaymentAuthor from "./Payment/PaymentAuthor";
import Auths from "./Author/useAuths";

function App() {
  // const token = localStorage.getItem("accessToken");
  // if (!token) {
  //   return <Dashboard />;
  // }
  return (
    <div className="wrapper">
      <Router>
        <div>
          <Route exact path="/">
            <br />
            <h1>Test Role</h1>
            <hr />
            <a href="/admin">
              <button>Admin</button>
            </a>
            <br />
            <br />
            <a href="/finance">
              <button>Finance</button>
            </a>
            <hr />
            <a href="/auth">
              <button>Author</button>
            </a>
            <hr />
            <a href="/regirationfee">
              <button>RegirationFee</button>
            </a>
            <hr />
            <a href="/auths">
              <button>TEST POST FORM TO BACKEND</button>
            </a>
          </Route>
        </div>
        <div>
          {/* admin */}
          <div>
            <Route exact path="/admin" component={Signin} />
            <Router>
              <div>
                <Route
                  exact
                  path="/admin/dashboard"
                  component={Dashboard}
                ></Route>
                <Route
                  exact
                  path="/admin/dashboard/newconference"
                  component={Newconference}
                />
                <Route
                  exact
                  path="/admin/dashboard/detail"
                  component={ConferenceDetail}
                ></Route>
                <Route
                  exact
                  path="/admin/dashboard/detail/:id"
                  component={PaperDetail}
                ></Route>
                <Route exact path="/admin/newpaper" component={UPaper}></Route>
              </div>
            </Router>
          </div>
          {/* Finance */}
          <div>
            <Route exact path="/finance" component={FDashboard} />
            <Router>
              <div>
                <Route exact path="/finance/dashboard" component={FDashboard} />
              </div>
            </Router>
          </div>
          {/* Author */}
          <div>
            <Route exact path="/auth" component={Auth}></Route>
            <Route exact path="/auth/paper" component={Paper}></Route>
            <Route exact path="/auth/paper/payment" component={PaymentAuthor} />
            <Route exact path="/auths" component={Auths} />
          </div>
          {/* regiration fee */}
          <div>
            <Route
              exact
              path="/regirationfee"
              component={Regirationfee}
            ></Route>
            <Route
              exact
              path="/regirationfee/01member"
              component={Test}
            ></Route>
          </div>
          {/* TEST */}
          <div>
            <Route exact path="/auths">
              <Auths />
            </Route>
          </div>
        </div>
      </Router>
    </div>
  );
}

export default App;
