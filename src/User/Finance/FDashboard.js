import React from "react";
import FNavbars from "./FNavbars";
import MainTable from "../Dashboard/MainTable";
export default function FDashboard() {
  return (
    <div>
      <div>
        <FNavbars />
      </div>
      <br />
      <br />
      <div>
        <MainTable />
      </div>
    </div>
  );
}
