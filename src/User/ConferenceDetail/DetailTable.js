import React from "react";
import Table from "react-bootstrap/Table";

function DetailTable() {
  return (
    <div>
      <Table>
        <thead>
          <tr>
            <th>No.</th>
            <th>Paper ID</th>
            <th>Title</th>
            <th>Topic</th>
            <th>Status</th>
            <th>Detail</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>1570678168</td>
            <td>
              An RSSI-based weighting with accelerometers for real-time indoor
              positioning
            </td>
            <td>[Regular KST]: Intelligent Computer Networks and Systems.</td>
            <td>
              <p style={{ color: "green" }}>Confirm</p>
            </td>
            <td align="center">
              <a href="/admin/dashboard/detail/:id">
                <button class="btn btn-primary" type="submit">
                  Detail
                </button>
              </a>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>1570679741</td>
            <td>K-mean Index Learning for Multimedia Datasets</td>
            <td>[Regular KST]: Intelligent Applications.</td>
            <td>
              <p style={{ color: "red" }}>Cancel</p>
            </td>
            <td align="center">
              <button class="btn btn-primary" type="submit">
                Detail
              </button>
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

export default DetailTable;
