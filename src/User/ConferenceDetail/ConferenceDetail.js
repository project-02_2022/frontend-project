import React from "react";
import Button from "react-bootstrap/Button";
import Navbars from "../Dashboard/Navbars";
import "../Dashboard/Styles.css";
import DetailTable from "./DetailTable";

function ConferenceDetail() {
  return (
    <div>
      <Navbars />
      <br />
      <h3>Conferences Detail</h3>
      <hr />
      <br />
      <div class="ex1">???</div>
      <br />
      <div class="ex1">
        <table align="center">
          <tr>
            <th>Papers</th>
            <th />
            <th>
              <a href="/admin/newpaper">
                <Button variant="primary">Add new papers</Button>
              </a>
            </th>
            <th />
            <th>
              <Button variant="success">Send Password</Button>{" "}
            </th>
            <th>
              <Button variant="warning">Export Excel File</Button>
            </th>
          </tr>
        </table>
        <br />
        <div>
          <DetailTable />
        </div>
      </div>
    </div>
  );
}

export default ConferenceDetail;
