import React from "react";
import Navbars from "../Dashboard/Navbars";
import Button from "react-bootstrap/Button";
import "../Dashboard/Styles.css";
import Card from "react-bootstrap/Card";
import Dropdown from "react-bootstrap/Dropdown";

function PaperDetail() {
  return (
    <div>
      <Navbars />
      <br />
      <h3>Paper Detail</h3>
      <br />
      <table align="center">
        <div class="ex1">
          <Card>
            <Card.Body>
              <Card.Title>
                <table align="center">
                  <tr>
                    <th>Detail </th>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </tr>
                </table>
              </Card.Title>
              <div>
                <table>
                  <hr />
                  <table class="text-right">
                    <tr>
                      <th>Paper ID :</th>
                      <th>1570678168</th>
                    </tr>
                    <tr>
                      <th>Paper Title :</th>
                      <th>
                        An RSSI-based weighting with accelerometers for
                        real-time indoor positioning
                      </th>
                    </tr>
                  </table>
                  <br />
                  <hr />
                  <br />
                  <table class="text-right">
                    <tr>
                      <tr>
                        <th>Author : </th>
                        <th>1</th>
                        <th>Panuwat Dan-klang</th>
                        <th>Burapha University, Thailand</th>
                        <th>panuwat@eng.buu.ac.th</th>
                      </tr>
                      <tr>
                        <th></th>
                        <th>2</th>
                        <th>Thanaluk Pranekunakol</th>
                        <th>Burapha University, Thailand</th>
                        <th>tpranekunakol@gmail.com</th>
                      </tr>
                    </tr>
                  </table>
                </table>
              </div>
            </Card.Body>
          </Card>
          <br />
          <Card>
            <Card.Title>Payment</Card.Title>
            <hr />
          </Card>
          <br />
          <Card>
            <Card.Title>Paper Status</Card.Title>
            <hr />
            <table class="table">
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Camera-Ready : </td>
                  <td>
                    <ul class="nav nav-pills nav-fill">
                      <li class="nav-item">
                        <button class="btn btn-success" disabled>
                          Completed
                        </button>
                      </li>
                    </ul>
                  </td>
                  <td>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" variant="secondary">
                        Please select
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">
                          Completed
                        </Dropdown.Item>
                        <Dropdown.Item href="#/action-2">
                          Declined
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>IEEE eCF : </td>
                  <td>
                    <ul class="nav nav-pills nav-fill">
                      <li class="nav-item">
                        <button class="btn btn-success" disabled>
                          Completed
                        </button>
                      </li>
                    </ul>
                  </td>
                  <td>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" variant="secondary">
                        Please select
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">
                          Completed
                        </Dropdown.Item>
                        <Dropdown.Item href="#/action-2">NoEntry</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Presentation files : </td>
                  <td>
                    <ul class="nav nav-pills nav-fill">
                      <li class="nav-item">
                        <button class="btn btn-danger" disabled>
                          Completed
                        </button>
                      </li>
                    </ul>
                  </td>
                  <td>
                    <Dropdown>
                      <Dropdown.Toggle id="dropdown-basic" variant="secondary">
                        Please select
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">
                          Completed
                        </Dropdown.Item>
                        <Dropdown.Item href="#/action-2">
                          Declined
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </td>
                </tr>
              </tbody>
            </table>
          </Card>
          <br />
          <table align="center">
            <div>
              <th>
                <Button variant="success">Certificate</Button>{" "}
              </th>
            </div>
          </table>
          <div>
            <br></br>
            <Button variant="primary">UPDATE DOCUMENT STATUS</Button>{" "}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Button variant="danger">CANCEL</Button>{" "}
          </div>
        </div>
      </table>
    </div>
  );
}

export default PaperDetail;
