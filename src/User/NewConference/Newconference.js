import React from "react";
import Navbars from "../Dashboard/Navbars";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "../Dashboard/Styles.css";
import Dropdown from "react-bootstrap/Dropdown";

function Newconference() {
  return (
    <div>
      <div>
        <Navbars />
      </div>
      <br />
      <div>
        <h1>New Conference</h1>
        <hr />
      </div>
      <div>
        <div>
          <table align="center">
            <div class="card" style={{ width: "20rem" }}>
              <div>
                <table align="center">
                  <h3>Conference Detail</h3>
                  <tr>
                    <Form.Label>Conference Title</Form.Label>
                    <Form.Control type="text" />
                  </tr>
                  <tr>
                    <Form.Label>Abbreviation</Form.Label>
                    <Form.Control type="text" />
                  </tr>
                  <tr>
                    <Form.Label>Conference Email</Form.Label>
                    <Form.Control type="email" />
                  </tr>
                  <br />
                </table>
              </div>
            </div>
          </table>
        </div>
      </div>
      <br />
      <div>
        <table align="center">
          <div class="card" style={{ width: "100%" }}>
            <table
              align="center"
              // border={{ border: "1px" }}
            >
              <tr>
                <th></th>
                <th>
                  <h5>Registration due</h5>
                </th>
                <th></th>
              </tr>
              <br />
              <tr>
                <th>Start Month</th>
                <th>Start Date</th>
                <th>Start Year</th>
              </tr>
              <tr>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
              </tr>
              <br />
              <tr>
                <th></th>
                <th>
                  <h5>Conference Date</h5>
                </th>
                <th></th>
              </tr>
              <br />
              <tr>
                <th>End Month</th>
                <th>End Date</th>
                <th>End Year</th>
              </tr>
              <tr>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
              </tr>
            </table>
          </div>
        </table>
      </div>
      <br />
      <div>
        <table align="center">
          <div class="card" style={{ width: "100%" }}>
            <table
              align="center"
              // border={{ border: "1px" }}
            >
              <tr>
                <th></th>
                <th>
                  <h5>Conference Date</h5>
                </th>
                <th></th>
              </tr>
              <br />
              <tr>
                <th>Start Month</th>
                <th>Start Date</th>
                <th>Start Year</th>
              </tr>
              <tr>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
              </tr>
              <br />
              <tr>
                <th></th>
                <th>
                  <h5>Conference Date</h5>
                </th>
                <th></th>
              </tr>
              <br />
              <tr>
                <th>End Month</th>
                <th>End Date</th>
                <th>End Year</th>
              </tr>
              <tr>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
                <th>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      - Please select -
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </th>
              </tr>
            </table>
          </div>
        </table>
      </div>
      <br />
      <div>
        <table align="center">
          <tr>
            <th>
              <Button variant="success">Success</Button>
            </th>
            <th />
            <th>
              <Button variant="danger">Danger</Button>
            </th>
          </tr>
        </table>
      </div>
    </div>
  );
}

export default Newconference;
