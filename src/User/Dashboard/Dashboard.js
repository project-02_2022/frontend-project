import React from "react";
import MainTable from "./MainTable";
import Navbars from "./Navbars";
import Button from "react-bootstrap/Button";
import "./Styles.css";
function Dashboard() {
  return (
    <div>
      <div>
        <Navbars />
      </div>
      <br />
      <div class="d-flex flex-row-reverse">
        <div class="p-3">
          <Button variant="primary" href="/admin/dashboard/newconference">
            Create new conferences
          </Button>
        </div>
      </div>
      <div class="m-2 space-x-3">
        <MainTable />
      </div>
    </div>
  );
}

export default Dashboard;
