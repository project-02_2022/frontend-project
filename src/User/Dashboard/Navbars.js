import React from "react";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

function Navbars() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>iConFerence </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/admin/dashboard">Manage ConFerences</Nav.Link>
            <Nav.Link href="/admin/dashboard/newconference">
              Create ConFerences
            </Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link href="/">Signout </Nav.Link>
          </Nav>
          <Nav>
            <Nav>
              <Nav.Link>Admin</Nav.Link>
              <Nav.Link href="#profile">profile</Nav.Link>
              <Navbar.Text>name</Navbar.Text>
            </Nav>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Navbars;
