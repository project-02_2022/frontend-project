import React from "react";
import Table from "react-bootstrap/Table";

function MainTable() {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>No.</th>
          <th>Title</th>
          <th>Due</th>
          <th>Detail</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>
            The 2021-13th International Conference on Knowledge and Smart
            Technology
          </td>
          <td>2020-12-10 - 2020-12-25</td>
          <td align="center">
            <a href="/admin/dashboard/detail">
              <button class="btn btn-primary" type="submit">
                Detail
              </button>
            </a>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td>
            The 2022-14th International Conference on Knowledge and Smart
            Technology
          </td>
          <td>2021-12-10 - 2022-01-26</td>
          <td align="center">
            <button class="btn btn-primary" type="submit">
              Detail
            </button>
          </td>
        </tr>
      </tbody>
    </Table>
  );
}

export default MainTable;
