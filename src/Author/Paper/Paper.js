import React from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Paper() {
  function userClick() {
    alert("OK @200");
  }

  return (
    <div
      style={{
        width: "100%",
        height: "1000px",
      }}
    >
      <br />
      <div class="ex1">
        <h1>Paper</h1>
      </div>
      <br />
      <form onClick={userClick}>
        <table align="center">
          <div class="card" style={{ width: "50rem" }}>
            <table
              align="center"
              // border="1px"
            >
              <div>
                <div class="row justify-content-center">
                  <div class="col-auto">
                    <table class="table table-responsive">
                      <table>
                        <tr>
                          <th>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>Title Paper</Form.Label>
                              <Form.Control style={{ width: "600px" }} />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>Topic Paper</Form.Label>
                              <Form.Control />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>Paper Status</Form.Label>
                              <Form.Control />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>Paper Detail</Form.Label>
                              <textarea
                                class="form-control"
                                rows="5"
                                id="comment"
                              ></textarea>
                            </Form.Group>
                            <br />
                          </th>
                        </tr>
                      </table>
                    </table>
                  </div>
                </div>
                <br />
                <div>
                  <Button variant="success" href="/auth/paper/payment">
                    Success
                  </Button>
                  <br />
                  <br />
                </div>
              </div>
            </table>
          </div>
        </table>
      </form>
    </div>
  );
}

export default Paper;
