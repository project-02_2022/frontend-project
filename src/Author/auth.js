import React, { useState } from "react";
import "./Styles.css";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";
import Button from "react-bootstrap/Button";

function Auth() {
  const [member, setMember] = useState("");

  return (
    <div
      style={{
        backgroundImage: `url("https://wallpaperaccess.com/full/427852.jpg")`,
        width: "100%",
        height: "1000px",
      }}
    >
      <br />
      <div class="ex1">
        <h1>Auth</h1>
      </div>
      <br />
      <table align="center">
        <div class="card" style={{ width: "30rem" }}>
          <table
            align="center"
            // border="1px"
          >
            <div>
              {/* <form onSubmit={handleSubmit}> */}
              <div>
                <div class="row justify-content-center">
                  <div class="col-auto">
                    <table class="table table-responsive">
                      <table>
                        <tr>
                          <th>
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>usernmae</Form.Label>
                              <Form.Control placeholder="username" />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>surname</Form.Label>
                              <Form.Control placeholder="surname" />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>affiliation</Form.Label>
                              <Form.Control placeholder="affiliation" />
                            </Form.Group>

                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>email</Form.Label>
                              <Form.Control placeholder="email" />
                            </Form.Group>
                            <br />
                            <div>
                              <Dropdown>
                                <Dropdown.Toggle
                                  variant="secondary"
                                  id="dropdown-basic"
                                >
                                  - select -
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                  <Dropdown.Item
                                    onClick={() => setMember("Member")}
                                  >
                                    Member
                                  </Dropdown.Item>
                                  <Dropdown.Item
                                    onClick={() => setMember("No Member")}
                                  >
                                    No Member
                                  </Dropdown.Item>
                                </Dropdown.Menu>
                              </Dropdown>
                            </div>
                            <br />
                            <Form.Group
                              className="mb-3"
                              controlId="formGridAddress1"
                            >
                              <Form.Label>member : {member}</Form.Label>
                            </Form.Group>
                          </th>
                        </tr>
                      </table>
                    </table>
                  </div>
                </div>
                <br />
                <div>
                  <Button variant="success" href="/auth/paper">
                    Success
                  </Button>
                  <br />
                  <br />
                </div>
              </div>
              {/* </form> */}
            </div>
          </table>
        </div>
      </table>
    </div>
  );
}

export default Auth;
